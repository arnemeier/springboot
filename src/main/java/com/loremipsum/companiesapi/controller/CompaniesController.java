package com.loremipsum.companiesapi.controller;

import com.loremipsum.companiesapi.model.Company;
import com.loremipsum.companiesapi.model.OperationResult;
import com.loremipsum.companiesapi.repository.CompaniesRepository;
import com.loremipsum.companiesapi.service.CompaniesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import  com.loremipsum.companiesapi.model.Dog;

import java.util.List;
import java.util.TimeZone;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*")
public class CompaniesController {

    @Autowired
    private CompaniesRepository companiesRepository;

    @Autowired
    private CompaniesService companiesService;

    @GetMapping("/hello")
    public String helloWorld(){

        return "Hello World";
    }


    @GetMapping("/hello/{name}")
    public String getHelloWorld(@PathVariable("name") String name) {
        return "Hello, " + name + "!";
    }


    @GetMapping("/dog")
    public Dog[] getDog() {
        Dog myDog = new Dog();
        myDog.setName("Muki");
        myDog.setTailLength(56);
        Dog myDog2 = new Dog();
        myDog2.setName("Archie");
        myDog2.setTailLength(25);
        return new Dog[] {myDog, myDog2};
    }

    @GetMapping("/all")
    public List<Company> getCompanies(){
        return companiesRepository.getAllCompanies();
    }

    //get - single company by id
    @GetMapping("/{companyId}")
    public Company getOneCompany(@PathVariable("companyId") int id){
        return companiesRepository.getCompany(id);
    }

    //get - by name ->  ei ole mõtet get päringut teha, tühikud sees
    // tasub teha hoopis post-päring
    //probleem: bad request kui pole post-param kaasa antud. seda saab lahendada kui lisada @requestParam lisada ka defaultvalue=""
    @PostMapping("/all")
    public List<Company> getOneCompaniesByName(@RequestParam("companyName") String coName){
        return companiesRepository.getAllCompaniesByName(coName);
    }

    //delete
    @DeleteMapping("/{companyId}")
    public void deleteCompany(@PathVariable("companyId") int id){
        companiesService.deleteCompany(id);
    }

    //add
    @PostMapping("/add")
    public OperationResult addCompany(@RequestBody Company company){
        return companiesService.addCompany(company);
    }

    //POST / PUT, e update company
    //tagastama ei pea midagi
    @PutMapping("/update")
    public void updateCompany(@RequestBody Company company){
        companiesService.updateCompany(company);
    }

    //CORS


}
