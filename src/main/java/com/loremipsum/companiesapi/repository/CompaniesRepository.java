package com.loremipsum.companiesapi.repository;

import com.loremipsum.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class CompaniesRepository {

    @Autowired
//    kui spring käivitub, siis autowired asjadele leiab beans baasist just sellise asja
//    soov on kasutada objekti, jdbctemplate teeb selle, kuidas ise tahab
//    autowired on siis linkimine

    private JdbcTemplate jdbcTemplate;

    public List<Company> getAllCompanies(){
        return jdbcTemplate.query("SELECT * FROM company", (row,number) -> {
            return new Company(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("logo"),
                    row.getInt("employees"),
                    row.getString("established")
            );
        });
    }

    //küsimärk on placeholder jdbc template funktsioonis. selle saab siis asendada pärast sql päringut
    // sarnane souf meetodile
    public Company getCompany(int companyId){
        List<Company> companies =  jdbcTemplate.query(
                "SELECT * FROM company where id = ?",
                new Object[]{companyId},
                (row,number) -> {
            return new Company(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("logo"),
                    row.getInt("employees"),
                    row.getString("established")
            );
        });
        return companies.size() > 0 ? companies.get(0) : null;
    }

    public List<Company> getAllCompaniesByName(String companyName){
        return jdbcTemplate.query("SELECT * FROM company where `name` LIKE ?",
                new Object[]{"%" + companyName + "%"},
                (row,number) -> {
            return new Company(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("logo"),
                    row.getInt("employees"),
                    row.getString("established")
            );
        });
    }

    public boolean companyExists(int companyId){
        Integer count = jdbcTemplate.queryForObject("select count(*) from company where id = ?",
                new Object[] {companyId},
                Integer.class
                );
        return count != null && count > 0;
    }

    public void deleteCompany(int id){
        jdbcTemplate.update("DELETE FROM company where id = ?", id);
    }

    public int addCompany(Company company) {
        //loogika kus siis on tagastusena vaja vastloodud com id-d.
        // käib veits pikalt
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(
                    "INSERT INTO company (`name`, `logo`, `employees`, `established`) values (?, ?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, company.getName());
            ps.setString(2, company.getLogo());
            ps.setInt(3, company.getEmployees());
            ps.setString(4, company.getEstablished());
            return ps;
        }, keyHolder);
//        jdbcTemplate.update("INSERT INTO company (`name`, `logo`, `employees`, `established`) values (?, ?, ?, ?)",
//                company.getName(), company.getLogo(), company.getEmployees(), company.getEstablished()
//                );

        return keyHolder.getKey().intValue();
    }

    public void updateCompany(Company company){
        jdbcTemplate.update(
                "UPDATE company SET `name` = ?, `logo` = ?, `employees` = ?, `established` = ? where `id` = ?",
                company.getName(), company.getLogo(), company.getEmployees(), company.getEstablished(), company.getId()
                );

    };


}

