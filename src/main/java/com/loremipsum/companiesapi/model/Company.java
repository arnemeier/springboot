package com.loremipsum.companiesapi.model;

public class    Company {
    private int id;
    private String name;
    private String logo;
    private int employees;
    private String established;

    public Company() {
    }

    public Company(int id, String name, String logo, int employees, String established) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.employees = employees;
        this.established = established;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }

    public String getEstablished() {
        return established;
    }

    public void setEstablished(String established) {
        this.established = established;
    }
}
