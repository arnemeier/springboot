package com.loremipsum.companiesapi.model;

public class OperationResult {
    private boolean isSuccesss;
    private String message;
    private Integer id;

    public OperationResult(boolean isSuccesss, String message, Integer id) {
        this.isSuccesss = isSuccesss;
        this.message = message;
        this.id = id;
    }

    public boolean isSuccesss() {
        return isSuccesss;
    }

    public void setSuccesss(boolean successs) {
        isSuccesss = successs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
